package com.devcamp.task54b50.Controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;

import org.springframework.web.bind.annotation.RestController;

import com.devcamp.task54b50.Service.AccountService;
import com.devcamp.task54b50.models.Account;

@RestController
public class AccountController {
    @Autowired
    private AccountService accountService;
    @CrossOrigin
    @GetMapping("/account")
    public ArrayList<Account> getAccount() {
        ArrayList<Account> accounts = accountService.getAllAccount();

        return accounts;
    }
}
