package com.devcamp.task54b50.models;

public class Account {
    private String id;
    private Customer customer;
    private double balance;
    public Account(String id, Customer customer, double balance) {
        this.id = id;
        this.customer = customer;
        this.balance = balance;
    }
     public Account(String id, double balance) {
        this.id = id;
        
        this.balance = balance;
    }
    
    public void setCustomer(Customer customer) {
        this.customer = customer;
    }
    public double getBalance() {
        return balance;
    }
    public void setBalance(double balance) {
        this.balance = balance;
    }
    
    @Override
    public String toString() {
        return "Account [id=" + id + ", customer=" + customer + ", balance=" + balance + "]";
    }

    public String getCustomerName() {
        return this.customer.getName();
    }

    public Customer getCustomer() {
        return customer;
    }
    public Account deposit(double amount) {
        balance += amount;
        return this;
    }

    public Account widthdraw(double amount){
        if(balance >= amount){
            balance -= amount;
        }else{
            System.out.println("Không đủ số dư để rút.");
        }
        return this;
    }

}
