package com.devcamp.task54b50.Service;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.devcamp.task54b50.models.Customer;

@Service
public class CustomerService {
    Customer customer1 = new Customer(01, "Hùy", 10);
    Customer customer2 = new Customer(02, "Hùng", 20);
    Customer customer3 = new Customer(03, "Hoa", 30);

    public ArrayList<Customer> getAllCustomer() {
        ArrayList<Customer> allCustomers = new ArrayList<>();
        allCustomers.add(customer1);
        allCustomers.add(customer2);
        allCustomers.add(customer3);
        return allCustomers;
    }
}
