package com.devcamp.task54b50.Service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.task54b50.models.Account;

@Service
public class AccountService {
    @Autowired
    private CustomerService customerService;

    Account account1 = new Account("HD01", 10000);
    Account account2 = new Account("HD02", 10000);
    Account account3 = new Account("HD03", 10000);

    public ArrayList<Account> getAllAccount() {
        ArrayList<Account> accounts = new ArrayList<>();
        account1.setCustomer(customerService.getAllCustomer().get(0));
        account2.setCustomer(customerService.getAllCustomer().get(1));
        account3.setCustomer(customerService.getAllCustomer().get(2));

        accounts.add(account1);
        accounts.add(account2);
        accounts.add(account3);
        return accounts;

    }
}
